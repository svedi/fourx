# Fourx

## Installation

```sh
$ yarn install
$ yarn global add parcel-bundler
```

## How to run

```sh
$ parcel index.html
```
