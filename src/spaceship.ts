import * as BABYLON from "babylonjs";

class Spaceship {
  public _mesh: BABYLON.Mesh;
  // public _pivot: BABYLON.Mesh;
  public _velocity = 0;
  public _steeringAngle = 0;

  constructor(scene: BABYLON.Scene) {
    const bodyMaterial = new BABYLON.StandardMaterial("body_mat", scene);
    bodyMaterial.diffuseColor = new BABYLON.Color3(1.0, 0.25, 0.25);
    bodyMaterial.backFaceCulling = false;

    const side = [
      new BABYLON.Vector3(-4, 2, -2),
      new BABYLON.Vector3(4, 2, -2),
      new BABYLON.Vector3(5, -2, -2),
      new BABYLON.Vector3(-7, -2, -2),
    ];

    side.push(side[0]); // close trapezium

    const extrudePath = [new BABYLON.Vector3(0, 0, 0), new BABYLON.Vector3(0, 0, 4)];

    this._mesh = BABYLON.MeshBuilder.ExtrudeShape(
      "body", { shape: side, path: extrudePath, cap : BABYLON.Mesh.CAP_ALL }, scene,
    );
    this._mesh.material = bodyMaterial;

    this._mesh.position = new BABYLON.Vector3(0, 0, 0);
  }

  public accelerate(): void {
    if (this._velocity < 2 ) {
      this._velocity += 0.18;
    }
  }

  public update(): void {
    if (this._velocity > 0.15) {
      this._velocity -= 0.15;
    } else {
      this._velocity = 0;
    }
  }
}

export default Spaceship;
