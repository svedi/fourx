import {
  ActionManager, Axis, Color3, Engine, ExecuteCodeAction, FreeCamera, HemisphericLight, Light, MeshBuilder,
  Particle, Scene, SolidParticleSystem, Space, StandardMaterial, Vector3,
} from "babylonjs";
import Spaceship from "./spaceship";

class Game {
  private _canvas: HTMLCanvasElement;
  private _engine: Engine;
  private _scene: Scene;
  private _camera: FreeCamera;
  private _light: Light;
  private _spaceship: Spaceship;

  constructor(canvasElement: string) {
    // Create canvas and engine.
    this._canvas = document.getElementById(canvasElement) as HTMLCanvasElement;
    this._engine = new Engine(this._canvas, true);
    // Create a basic BJS Scene object.
    this._scene = new Scene(this._engine);

    // Create a FreeCamera, and set its position to (x:0, y:5, z:-10).
    this._camera = new FreeCamera("camera1", new Vector3(0, 5, -10), this._scene);

    // Create a basic light, aiming 0,1,0 - meaning, to the sky.
    this._light = new HemisphericLight("light1", new Vector3(0, 1, 0), this._scene);

    this._spaceship = new Spaceship(this._scene);
  }

  public createScene(): void {
    // Target the camera to scene origin.
    this._camera.setTarget(Vector3.Zero());

    // Attach the camera to the canvas.
    this._camera.attachControl(this._canvas, false);

    /*****************************Add Ground********************************************/
    const groundSize = 400;

    // Create a built-in "ground" shape.
    const ground = MeshBuilder.CreateGround("ground", {width: groundSize, height: groundSize}, this._scene);
    const groundMaterial = new StandardMaterial("ground", this._scene);
    groundMaterial.diffuseColor = new Color3(0.75, 1, 0.25);
    ground.material = groundMaterial;
    ground.position.y = -1.5;
    /*****************************End Add Ground********************************************/

    /*****************************Particles to Show Movement********************************************/
    const box = MeshBuilder.CreateBox("box", {}, this._scene);
    box.position = new Vector3(20, 0, 10);

    const boxesSPS = new SolidParticleSystem("boxes", this._scene, {updatable: false});

    // function to position of grey boxes
    const setBoxes = (particle: Particle) => {
      particle.position = new Vector3(-200 + Math.random() * 400, 0, -200 + Math.random() * 400);
    };

    // add 400 boxes
    boxesSPS.addShape(box, 400, {positionFunction: setBoxes});
    const boxes = boxesSPS.buildMesh(); // mesh of boxes
    boxes.material = new StandardMaterial("", this._scene);
    boxes.material.alpha = 0.25;
  }

  public activateSpaceship(): void {
    const map: { [x: string]: boolean } = {};

    this._scene.actionManager = new ActionManager(this._scene);

    this._scene.actionManager.registerAction(
      new ExecuteCodeAction(
        ActionManager.OnKeyDownTrigger,
        (evt) => {
          map[evt.sourceEvent.key] = evt.sourceEvent.type === "keydown";
        },
      ),
    );

    this._scene.actionManager.registerAction(
      new ExecuteCodeAction(
        ActionManager.OnKeyUpTrigger, (evt) => {
          map[evt.sourceEvent.key] = evt.sourceEvent.type === "keydown";
        },
      ),
    );

    this._scene.registerAfterRender(() => {
      const F = this._engine.getFps();

      if (map[" "] || map["w"] || map["W"]) {
        this._spaceship.accelerate();
      }

      this._spaceship.update();

      if ((map["a"] || map["A"]) && -Math.PI / 6 < this._spaceship._steeringAngle) {
        this._spaceship._mesh.rotatePOV(0, -0.05, 0);
      }

      if ((map["d"] || map["D"]) && Math.PI / 6 > this._spaceship._steeringAngle) {
        this._spaceship._mesh.rotatePOV(0, +0.05, 0);
      }

      this._spaceship._mesh
        .movePOV(0, 0, this._spaceship._velocity);
    });
  }

  public doRender(): void {
    // Run the render loop.
    this._engine.runRenderLoop(() => {
      this._scene.render();
    });

    // The canvas/window resize event handler.
    window.addEventListener("resize", () => {
      this._engine.resize();
    });
  }
}

export default Game;
