import Game from "./src/game";

window.addEventListener("DOMContentLoaded", () => {
  // Create the game using the 'renderCanvas'.
  const game = new Game("renderCanvas");

  // Create the scene.
  game.createScene();

  game.activateSpaceship();

  // Start render loop.
  game.doRender();
});
